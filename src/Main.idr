module Main

%default total

data TyExp : Type where
  TyNat : TyExp
  TyBool : TyExp

val : TyExp -> Type
val TyNat = Nat
val TyBool = Bool

data Exp : TyExp -> Type where
  Val : val t -> Exp t
  Plus : (e0: Exp TyNat) -> (e1: Exp TyNat) -> Exp TyNat
  If : (b : Exp TyBool) -> (e0 : Exp t) -> (e1 : Exp t) -> Exp t

eval : Exp t -> val t
eval (Val v) = v
eval (Plus e0 e1) = eval e0 + eval e1
eval (If b e0 e1) = case eval b of
  True => eval e0
  False => eval e1

StackType : Type
StackType = List TyExp

data Stack : StackType -> Type where
  Nil : Stack []
  (::) : val t -> Stack s -> Stack (t :: s)

top : Stack (t :: s) -> val t
top (v :: _) = v

tail : Stack (t :: s) -> Stack s
tail (_ :: rest) = rest

consInjective : {x : val t} -> {xs : Stack s} -> {y : val t} -> {ys : Stack s} ->
                (x :: xs) = (y :: ys) -> (x = y, xs = ys)
consInjective Refl = (Refl, Refl)

data Code : StackType -> StackType -> Type where
  SKIP : Code s s
  (++) : Code s0 s1 -> Code s1 s2 -> Code s0 s2
  PUSH : val t -> Code s (t :: s)
  ADD : Code (TyNat :: TyNat :: s) (TyNat :: s)
  IF : Code s s' -> Code s s' -> Code (TyBool :: s) s'

exec : Code s s' -> Stack s -> Stack s'
exec SKIP s = s
exec (c0 ++ c1) s = exec c1 (exec c0 s)
exec (PUSH v) s = v :: s
exec ADD (n :: m :: s) = (n + m) :: s
exec (IF c0 c1) (b :: s) = case b of
  True => exec c0 s
  False => exec c1 s

compile : Exp t -> Code s (t :: s)
compile (Val v) = PUSH v
compile (Plus e0 e1) = compile e1 ++ compile e0 ++ ADD
compile (If b e0 e1) = compile b ++ IF (compile e0) (compile e1)

correct : (e : Exp t) -> (s : Stack st) -> (eval e :: s) = (exec (compile e) s)
correct (Val x) s = Refl
correct (Plus e0 e1) s =
  let e1ih = sym (correct e1 s) in
  let e0ih = sym (correct e0 (exec (compile e1) s)) in
  rewrite e0ih in
  rewrite e1ih in
  Refl
correct (If b e0 e1) s with (eval b) proof eb
  correct (If b e0 e1) s | True with (exec (compile b) s) proof cb
    correct (If b e0 e1) s | True | (True :: s') =
      let e0ih = sym (correct e0 s') in
      let bih = sym (correct b s) in
      rewrite e0ih in
      let x = trans cb bih in
      let (y, z) = consInjective x in
      rewrite z in
      Refl
    -- TODO: can i do this a different way so these voids aren't necessary?
    correct (If b e0 e1) s | True | (False :: s') =
      let bih = sym (correct b s) in
      let x = trans cb bih in
      let (y, z) = consInjective x in
      let a = trans y (sym eb) in
      void (trueNotFalse (sym a))
  correct (If b e0 e1) s | False with (exec (compile b) s) proof cb
    correct (If b e0 e1) s | False | (False :: s') =
      let e1ih = sym (correct e1 s') in
      let bih = sym (correct b s) in
      rewrite e1ih in
      let x = trans cb bih in
      let (y, z) = consInjective x in
      rewrite z in
      Refl
    -- TODO: can i do this a different way so these voids aren't necessary?
    correct (If b e0 e1) s | False | (True :: s') =
      let bih = sym (correct b s) in
      let x = trans cb bih in
      let (y, z) = consInjective x in
      let a = trans y (sym eb) in
      void (trueNotFalse a)

export
main : IO ()
main = putStrLn "hello"
